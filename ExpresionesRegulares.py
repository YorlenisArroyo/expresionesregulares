import re

palabra= input("Palabra a Evaluar: ")

if re.match('casa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('.asa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#
if re.match('.a.a', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

# que tenga un punto adelante

if re.match('\.a.a', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#varias condiciones que cumpla con 1 de estos es valido

if re.match('jpg|png|gif', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#por grupos
if re.match('ca(..|...|)ta', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#
if re.match('\.(jpg|png|gif)', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#  letras u numeros
if re.match('R[0-9]D[0-9]', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca[a-z]a', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca[a-c ]a', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#solo 1 de las dos condiciones
if re.match('ca[a-c0-5 ]a', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#
if re.match('ca\da', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca\Da', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#
if re.match('ca\wa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca\Wa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca\sa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")
#
if re.match('ca\Sa', palabra):
    print("coincidencia")
else:
    print("sin concidencia")

#Repeticiones
#+ * ? []

palabra= input("Palabra a Evaluar: ")

if re.match('[A-Z][a-z]+\s[A-Z](\.|[a-z]*)', palabra):
    print("coincidencia")
else:
    print("sin concidencia")